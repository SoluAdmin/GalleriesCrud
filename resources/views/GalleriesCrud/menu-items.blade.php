@if(\Auth::user()->hasModule('SoluAdmin\\GalleriesCrud'))
<li>
    <a href="{{ url(config('backpack.base.route_prefix', 'admin') . config('SoluAdmin.GalleriesCrud.route_prefix', '') . '/gallery') }}">
        <i class="fa fa-photo"></i> <span>{{trans('SoluAdmin::GalleriesCrud.gallery_plural')}}</span>
    </a>
</li>
@endif