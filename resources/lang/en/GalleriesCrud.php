<?php

return [
    'gallery_singular' => 'Gallery',
    'gallery_plural' => 'Galleries',
    'name' => 'Name',
];
