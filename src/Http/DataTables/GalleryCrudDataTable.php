<?php

namespace SoluAdmin\GalleriesCrud\Http\DataTables;

use SoluAdmin\Support\Interfaces\DataTable;

class GalleryCrudDataTable implements DataTable
{
    public function columns()
    {
        return [
            [
                'name' => 'name',
                'label' => trans('SoluAdmin::GalleriesCrud.name'),
            ],
        ];
    }
}
