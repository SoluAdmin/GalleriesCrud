<?php

namespace SoluAdmin\GalleriesCrud\Http\Forms;

use SoluAdmin\Support\Interfaces\Form;

class GalleryCrudForm implements Form
{
    public function fields()
    {
        return [
            [
                'name' => 'name',
                'label' => trans('SoluAdmin::GalleriesCrud.name'),
            ],
        ];
    }
}
