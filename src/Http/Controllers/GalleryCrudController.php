<?php

namespace SoluAdmin\GalleriesCrud\Http\Controllers;

use SoluAdmin\Support\Http\Controllers\BaseCrudController;
use SoluAdmin\GalleriesCrud\Http\Requests\GalleryCrudRequest as StoreRequest;
use SoluAdmin\GalleriesCrud\Http\Requests\GalleryCrudRequest as UpdateRequest;
use SoluAdmin\GalleriesCrud\Models\Gallery;

class GalleryCrudController extends BaseCrudController
{

    public function setup()
    {
        parent::setup();
    }

    public function store(StoreRequest $request)
    {
        return parent::storeCrud();
    }

    public function update(UpdateRequest $request)
    {
        return parent::updateCrud();
    }
}
