<?php

namespace SoluAdmin\GalleriesCrud\Models;

use Backpack\CRUD\CrudTrait;
use Backpack\CRUD\ModelTraits\SpatieTranslatable\HasTranslations;
use Illuminate\Database\Eloquent\Model;

class Gallery extends Model
{
    use CrudTrait;
    use HasTranslations;

    protected $guarded = ['id'];

    protected $translatable = ['name'];
}
